# Generated by Django 4.0.6 on 2022-07-05 21:49

from django.db import migrations, models
import uuid


def gen_uuid(apps, schema_editor):
    BookModel = apps.get_model('book', 'Book')
    for row in BookModel.objects.all():
        row.uuid = uuid.uuid4()
        row.save(update_fields=['bar_code'])


class Migration(migrations.Migration):
    dependencies = [
        ('book', '0002_book_bar_code'),
    ]

    operations = [
        migrations.RunPython(gen_uuid, reverse_code=migrations.RunPython.noop),
    ]
