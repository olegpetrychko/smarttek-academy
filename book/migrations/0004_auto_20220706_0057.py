# Generated by Django 4.0.6 on 2022-07-05 21:57

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0003_auto_20220706_0049'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='bar_code',
            field=models.UUIDField(default=uuid.uuid4, null=False),
        ),
    ]
