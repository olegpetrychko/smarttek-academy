from django.db import models
import uuid


# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=32)
    description = models.TextField()
    bar_code = models.UUIDField(default=uuid.uuid4, unique=True)

    def __str__(self):
        return f'{self.title}'
