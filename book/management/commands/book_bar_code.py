from django.core.management.base import BaseCommand, CommandError
from book.models import Book
from django.core.exceptions import ObjectDoesNotExist


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('book_ids', nargs='+', type=int)

    def handle(self, *args, **kwargs):
        print(f'Start command book_bar_code')
        for book_id in kwargs['book_ids']:
            try:
                book = Book.objects.get(pk=book_id)
            except ObjectDoesNotExist:
                raise CommandError(f'Book with ID {book_id} does not exist')

            self.stdout.write(self.style.SUCCESS(f'Book - {book_id} bar_code {book.bar_code}'))
